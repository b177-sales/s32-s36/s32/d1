const express = require("express");
const router = express.Router();
const courseController = require("../controllers/course");
const auth = require("../auth");

// Route for creating a course
router.post("/", (req, res) => {
	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
})

router.get("/courses", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));

});

// Route for retrieveing all courses
router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
})

// Route for retrieving specific course
router.get("/:courseId", (req, res) => {
	console.log(req.params.courseId);

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})

// route for updating a course
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
})

// Archive Course
router.put("/:courseId", auth.verify, (req, res) => {

	const archive = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
})

module.exports = router;
